"use strict";

var savePoll = function(issue, poll) {
  // add the glance update, see https://developer.atlassian.com/cloud/jira/platform/modules/issue-glance/
  AP.request("/rest/api/3/issue/" + issue + "/properties/com.atlassian.jira.issue:com.eric.atlassian-poll:my-issue-glance:status", {
    type: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify({type: 'badge', value: {label: poll.answers.reduce((t, e) => t + e.count, 0)}})
  });

  // AP.request() https://developer.atlassian.com/cloud/jira/platform/jsapi/request/
  AP.request("/rest/api/3/issue/" + issue + "/properties/poll", { // store as issue property
    type: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(poll),
    success: () => window.location.reload(false) // reload so we get the poll displayed
  });
};

AP.context.getContext(ctx => {
  $(document).on('submit', '#create form', () => { // save the poll
    var poll = {
      question: $("#create form [name=question]").val(),
      answers: $("#create form [name=answer]").map((i, e) => ({answer: $(e).val(), count: 0})).get()
    }

    savePoll(ctx.jira.issue.key, poll);
    return false;
  }).on('submit', '#poll form', () => { // submit a poll vote
    var poll = $('#poll').data('poll'), index = $('#poll form [name=answer]:checked').data('index');
    poll.answers[index].count++;
    savePoll(ctx.jira.issue.key, poll);
    return false;
  });

  AP.request("/rest/api/3/issue/" + ctx.jira.issue.key + "/properties/poll", { // load the poll
    success: p => {
      var poll = JSON.parse(p).value, ui = $("#poll");
      ui.data('poll', poll);

      ui.find('.question').text(poll.question);
      ui.find('div.radio').each((i, e) => {
        if (poll.answers[i].answer) {
          $(e).find('label').text(poll.answers[i].answer);
          $(e).find('aui-badge').text(poll.answers[i].count);
        } else {
          $(e).closest('.radio').remove();
        }
      });

      $('#poll').show();
    },
    error: () => { // nothing exists:
      $('#create').show();
    }
  });
});