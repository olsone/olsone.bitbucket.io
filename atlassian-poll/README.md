atlassian-poll
==============
This project contains a demo Connect app for Jira.  The application code is intentionally _extremely_ simple to showcase the specific aspects that make up a Jira Connect app.  It is intended to be used as an educational tool or extended with more complete functionality to make a real app.

The app itself is a simple poll presented as an issue glance.  This would allow to create a specific poll question for each Jira issue (think something like "Should we release this issue?" or "How important is this to get done?")

Installation
------------
1. Make sure you have [a free developer instance of Jira to install the app on](http://go.atlassian.com/cloud-dev).
2. Install the app with the following URL: https://olsone.bitbucket.io/atlassian-poll/atlassian-connect-jira.json

Usage
-----
1. Navigate to an issue and create the poll with the `Poll Question` glance in the issue detail view.
2. Start voting!

Next Steps
----------
There are a number of steps that could be taken to make this a better app.  These are left as excercises in subsequent versions.  Feel free to fork your own version to add any missing functions, or use this project as a base for a real app of your own!  Some ideas:

* Tests!  There are no unit/integration tests for any of the functions provided here.  For a real, production-level app these are a requirement.
* No way to remove or reset an existing poll
* No way to edit the options of a poll
* Polls do not keep track of who has voted, so votes can be submitted any number of times
* Better visualization for the poll data
* Use [React](https://reactjs.org/)/[Atlaskit](https://atlaskit.atlassian.com/) to build a more responsive UI using components!
* By [indexing issue properties, it would be possible to build powerful JQL queries to aggregate poll data in reports](https://developer.atlassian.com/cloud/jira/platform/modules/entity-property/).
* Concurrency issues if multiple people vote at the same time
* Show leading response rather than total votes in glance?
* Publish on Marketplace!  This would allow customers to actually find and install (and potentially pay for) our app.

Key Points
----------
1. There are _many_ other extension points available in Jira!  We are adding an issue glance, but there are many other possibilities here!
1. The app uses the [*.bitbucket.io base url so that we don't have to worry about hosting the app on a server or CDN](https://confluence.atlassian.com/bitbucket/publishing-a-website-on-bitbucket-cloud-221449776.html)
2. The app uses [issue property storage to host data](https://developer.atlassian.com/cloud/jira/platform/storing-data-with-entity-properties/).  This means we don't need to have any server-side data storage even though it does store data

Important Links
---------------
* [Jira Cloud Developer Guide](https://developer.atlassian.com/cloud/jira/platform/)
* [Jira Cloud REST API](https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-group-Issue-properties)
* [Atlassian Cloud Developer Instance](http://go.atlassian.com/cloud-dev)